import React, { Component } from 'react';

import {
  Banner,
  AboutMe,
  Experience,
  Projects,
  Skills,
  Contact,
} from './components';

class App extends Component {
  render() {
    return (
      <div className="main-container">
        <Banner />
        <AboutMe />
        <Experience />
        <Projects />
        <Skills />
        <Contact />
      </div>
    );
  }
}

export default App;