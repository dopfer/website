import React from 'react';
import { Link } from 'react-scroll';
import { motion } from 'framer-motion';

import styles from './Banner.module.scss';

export const JumpDownArrow = () => {

    const scrollProps = {
        to: "afterParallax",
        smooth: true,
        duration: 1500,
    };

    return(
        <motion.div animate={{y: [0.1, 15]}} transition={{ yoyo: Infinity, duration: 1.25, ease: "easeIn" }}>
            <Link {...scrollProps}>
                <i className={styles.arrowDown} />
            </Link>
        </motion.div>
    )
}
