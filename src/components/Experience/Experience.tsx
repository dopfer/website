import React from 'react';
import { motion } from 'framer-motion';
import { InView } from 'react-intersection-observer';
import { MotionContainer } from '../../util/MotionContainer';
import ExperienceItem from './ExperienceItem';

import styles from './Experience.module.scss';

function Experience() {

    const textAnimTransition = { type: "spring", stiffness: 260, damping: 20 }

    const options = {
        threshold: 0.3,
        triggerOnce: true,
    }

    return(
        <div className="content-container">
            <div className="content">
                <h2>Experience</h2>

                <InView {...options}>
                    {({ inView, ref }) => (
                        <MotionContainer ref={ref}>
                            <motion.div ref = {ref} className={styles.fling} animate={{ opacity: inView ? 1 : 0, right: inView ? 0 : -10 }} transition={ textAnimTransition }>
                                <h3>Career</h3>
                                {/* Mention sitecore, Visual studio, devops? */}
                                <ExperienceItem 
                                    job="ActewAGL"
                                    date="2020 - Now"
                                    workTitle="API Developer"
                                    description="Creating and maintaining web and database API's using .NET, WCF and SQL Server."
                                />
                                <ExperienceItem 
                                    job="ActewAGL"
                                    date="2020"
                                    workTitle="Digital Intern"
                                    description="Created a data pipeline to read CSV files into a SQL Server 
                                    database using SSIS, then deployed interactive reports using SSRS. 
                                    This software improved productivity by automating a time 
                                    consuming manual process."
                                />
                                <ExperienceItem 
                                    job="Department of Primary Industries (DPI)"
                                    date="2019"
                                    workTitle="Technical Life Cycle Assistant"
                                    description="Created an interactive graph using Python and Excel, to dynamically
                                    display crop data and respond to user input. This software read an 
                                    excel file of crop data and graphed the data in a web browser, 
                                    implementing various dropdowns and checkboxes which interfaced 
                                    live with the data."
                                />
                                <ExperienceItem 
                                    job="Envirowest Consulting"
                                    date="2014"
                                    workTitle="Laboratory Assistant"
                                    description="Collected field samples of soil and performed 
                                    laboratory tests for optimal soil conditions. 
                                    These tests were undertaken with industry standards and 
                                    reported in a standardised format."
                                />
                            </motion.div>
                        </MotionContainer>
                    )}
                </InView>
                
                <InView {...options}>
                    {({ inView, ref }) => (
                        <MotionContainer ref={ref}>
                            <motion.div ref = {ref} className={styles.fling} animate={{ opacity: inView ? 1 : 0, right: inView ? 0 : -10  }} transition={ textAnimTransition }>
                                <h3>Education</h3>
                                <ExperienceItem 
                                    job="Australian National University (ANU)"
                                    date="2015-2020"
                                    workTitle="Bachelor of Software Engineering (Honours)"
                                    description="The main skills I have gained are in frontend and backend systems, developing 
                                    web based user interfaces and using embedded devices to serve and capture data."
                                />
                            </motion.div>
                        </MotionContainer>
                    )}
                </InView>

                

                <InView {...options}>
                    {({ inView, ref }) => (
                        <MotionContainer ref={ref}>
                            <motion.div ref = {ref} className={styles.fling} animate={{ opacity: inView ? 1 : 0, right: inView ? 0 : -10 }} transition={ textAnimTransition }>
                                <h3>University Projects</h3>
                                <ExperienceItem 
                                    job="Farmbot Farm Designer"
                                    date="2019"
                                    workTitle="Team Leader / Developer"
                                    description="I led a team for a year to design programs for designing 
                                    sustainable farms. Besides managing, I worked on software 
                                    to generate farm designs for a Farmbot machine, as well 
                                    as an Angular web interface to plan sustainable farms on a 
                                    larger scale, hosted on Github Pages."
                                />
                                <ExperienceItem 
                                    job="Elementice"
                                    date="2018"
                                    workTitle="Node.js Backend Developer"
                                    description="Worked with Elementice to develop a 360° camera rig to 
                                    capture still motion images. I developed 
                                    a NodeJS backend to coordinate image capture and video 
                                    streaming across multiple cameras, and a web front end 
                                    to trigger image capture and view the video stream."
                                />
                                <ExperienceItem 
                                    job="Clearbox Systems"
                                    date="2018"
                                    workTitle="Typescript Frontend Developer"
                                    description="Worked with Clearbox Systems to create a web interface 
                                    to monitor a live radio spectrum simulating 
                                    radar data. I worked in the frontend team to develop 
                                    the web interface in Typescript."
                                />
                            </motion.div>
                        </MotionContainer>
                    )}
                </InView>
            </div>
        </div>
    )
}

export { Experience };