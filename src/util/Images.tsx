const profile = [
    { src: process.env.PUBLIC_URL + "/images/profile/160.jpg", width: "160w" },
    { src: process.env.PUBLIC_URL + "/images/profile/369.jpg", width: "369w" },
    { src: process.env.PUBLIC_URL + "/images/profile/528.jpg", width: "528w" },
    { src: process.env.PUBLIC_URL + "/images/profile/662.jpg", width: "662w" },
    { src: process.env.PUBLIC_URL + "/images/profile/780.jpg", width: "780w" },
    { src: process.env.PUBLIC_URL + "/images/profile/895.jpg", width: "895w" },
    { src: process.env.PUBLIC_URL + "/images/profile/997.jpg", width: "997w" },
    { src: process.env.PUBLIC_URL + "/images/profile/1080.jpg", width: "1080w" }
];
const profileSrcSet = profile
    .map(image => `${image.src} ${image.width}`)
    .join(",");

const appInput = [
    { src: process.env.PUBLIC_URL + "/images/projects/app/input/160.png", width: "160w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/input/342.png", width: "342w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/input/474.png", width: "474w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/input/585.png", width: "585w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/input/682.png", width: "682w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/input/771.png", width: "771w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/input/858.png", width: "858w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/input/941.png", width: "941w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/input/1017.png", width: "1017w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/input/1080.png", width: "1080w" },
];
const appInputSrcSet = appInput
    .map(image => `${image.src} ${image.width}`)
    .join(",");

const appMain = [
    { src: process.env.PUBLIC_URL + "/images/projects/app/main/160.png", width: "160w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/main/343.png", width: "343w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/main/474.png", width: "474w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/main/585.png", width: "585w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/main/682.png", width: "682w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/main/771.png", width: "771w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/main/858.png", width: "858w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/main/942.png", width: "942w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/main/1017.png", width: "1017w" },
    { src: process.env.PUBLIC_URL + "/images/projects/app/main/1080.png", width: "1080w" },
];
const appMainSrcSet = appMain
    .map(image => `${image.src} ${image.width}`)
    .join(",");

const appSettings = [
        { src: process.env.PUBLIC_URL + "/images/projects/app/settings/160.png", width: "160w" },
        { src: process.env.PUBLIC_URL + "/images/projects/app/settings/342.png", width: "343w" },
        { src: process.env.PUBLIC_URL + "/images/projects/app/settings/473.png", width: "474w" },
        { src: process.env.PUBLIC_URL + "/images/projects/app/settings/583.png", width: "585w" },
        { src: process.env.PUBLIC_URL + "/images/projects/app/settings/681.png", width: "682w" },
        { src: process.env.PUBLIC_URL + "/images/projects/app/settings/771.png", width: "771w" },
        { src: process.env.PUBLIC_URL + "/images/projects/app/settings/856.png", width: "858w" },
        { src: process.env.PUBLIC_URL + "/images/projects/app/settings/940.png", width: "942w" },
        { src: process.env.PUBLIC_URL + "/images/projects/app/settings/1016.png", width: "1017w" },
        { src: process.env.PUBLIC_URL + "/images/projects/app/settings/1080.png", width: "1080w" },
];
const appSettingsSrcSet = appSettings
    .map(image => `${image.src} ${image.width}`)
    .join(",");

const website = [
        { src: process.env.PUBLIC_URL + "/images/projects/website/160.png", width: "160w" },
        { src: process.env.PUBLIC_URL + "/images/projects/website/418.png", width: "418w" },
        { src: process.env.PUBLIC_URL + "/images/projects/website/617.png", width: "617w" },
        { src: process.env.PUBLIC_URL + "/images/projects/website/795.png", width: "795w" },
        { src: process.env.PUBLIC_URL + "/images/projects/website/1080.png", width: "1080w" },
];
const websiteSrcSet = website
    .map(image => `${image.src} ${image.width}`)
    .join(",");

const gitlabIcon = process.env.PUBLIC_URL + "/images/icons/gitlab-icon-rgb.svg";

const parallax = [
    {src: process.env.PUBLIC_URL + "/images/parallax/layer0.svg", parallaxSpeed: "-13", parallaxZIndex: "-11"},
    {src: process.env.PUBLIC_URL + "/images/parallax/layer1.svg", parallaxSpeed: "-12", parallaxZIndex: "-10"},
    // << Title Location >>
    {src: process.env.PUBLIC_URL + "/images/parallax/layer2.svg", parallaxSpeed: "-11", parallaxZIndex: "-8"},
    {src: process.env.PUBLIC_URL + "/images/parallax/layer3.svg", parallaxSpeed: "-10", parallaxZIndex: "-7"},
    {src: process.env.PUBLIC_URL + "/images/parallax/layer4.svg", parallaxSpeed: "-8", parallaxZIndex: "-6"},
    {src: process.env.PUBLIC_URL + "/images/parallax/layer5.svg", parallaxSpeed: "-7", parallaxZIndex: "-5"},
    {src: process.env.PUBLIC_URL + "/images/parallax/layer6.svg", parallaxSpeed: "-6", parallaxZIndex: "-4"},
    {src: process.env.PUBLIC_URL + "/images/parallax/layer7.svg", parallaxSpeed: "-5", parallaxZIndex: "-3"},
    {src: process.env.PUBLIC_URL + "/images/parallax/layer8.svg", parallaxSpeed: "-4", parallaxZIndex: "-2"},
    {src: process.env.PUBLIC_URL + "/images/parallax/layer9.svg", parallaxSpeed: "-2", parallaxZIndex: "-1"},
    {src: process.env.PUBLIC_URL + "/images/parallax/layer10.svg", parallaxSpeed: "0", parallaxZIndex: "0"},
];

// const parallaxCoalesced = process.env.PUBLIC_URL + "/images/parallax/layersCoalesced.svg";

export { 
    profile, profileSrcSet, 
    appInput, appInputSrcSet,
    appMain, appMainSrcSet,
    appSettings, appSettingsSrcSet,
    website, websiteSrcSet,
    gitlabIcon, parallax
};